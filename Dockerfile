FROM clue/textract

WORKDIR /app

RUN apt-get install -y tesseract-ocr

COPY ./pdf /app

COPY ./parse.py /app

ENTRYPOINT ["python"]

CMD ["parse.py"]