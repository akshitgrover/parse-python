import textract
import os
import re

for i in os.listdir("."):
    if(i.endswith(".pdf")):
        text = textract.process(i,method="tesseract",language="eng")
        arr = text.split("\n")
        for t in arr:
            num = re.findall(r"[0-9]{10}",t)
            if(len(num) > 0):
                print(num[0])